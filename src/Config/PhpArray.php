<?php

namespace PB\PIV\TrackingService\Config;

class PhpArray implements ConfigInterface
{
    private $array;

    public function __construct($options)
    {
        $this->array = $options;
    }

    public function value($project, $key)
    {
        return empty($this->array[$project][$key]) ? null : $this->array[$project][$key];
    }

    public function projectExists($project)
    {
        return array_key_exists($project, $this->array);
    }
}