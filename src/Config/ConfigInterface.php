<?php

namespace PB\PIV\TrackingService\Config;

interface ConfigInterface
{
    public function value($project, $key);

    public function projectExists($project);
}