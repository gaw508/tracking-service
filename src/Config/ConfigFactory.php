<?php

namespace PB\PIV\TrackingService\Config;

class ConfigFactory
{
    public static function get($config_type, $options = array())
    {
        $class = "\\PB\\PIV\\TrackingService\\Config\\" . $config_type;
        return new $class($options);
    }
}