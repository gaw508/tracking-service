<?php

namespace PB\PIV\TrackingService;

use PB\PIV\TrackingService\Config\ConfigInterface;
use PB\PIV\TrackingService\DataSource\DataSourceFactory;

class Event
{
    private $config;
    private $project;

    public function __construct($project, ConfigInterface $config)
    {
        $this->project = $project;
        $this->config = $config;
    }

    public function validAction($action)
    {
        return in_array($action, $this->config->value($this->project, 'valid_actions'));
    }

    public function save($session_id, $action, $data = array())
    {
        $datasource = DataSourceFactory::get($this->config->value($this->project, 'datasource'));

        return $datasource->saveEvent($session_id, $action, $data);
    }
}