<?php

namespace PB\PIV\TrackingService;

use PB\PIV\TrackingService\Config\ConfigInterface;
use PB\PIV\TrackingService\DataSource\DataSourceFactory;

class Session
{
    private $config;
    private $project;

    public function __construct($project, ConfigInterface $config)
    {
        $this->project = $project;
        $this->config = $config;
    }

    public function exists($id)
    {
        $datasource = DataSourceFactory::get($this->config->value($this->project, 'datasource'));

        return $datasource->sessionExists($id);
    }

    public function save($data)
    {
        $datasource = DataSourceFactory::get($this->config->value($this->project, 'datasource'));

        return $datasource->saveSession($data);
    }
}