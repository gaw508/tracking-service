<?php

namespace PB\PIV\TrackingService\DataSource;

use Aws\DynamoDb\DynamoDbClient;

class DynamoDB implements DataSourceInterface
{
    private $config;
    private $credentials_array;

    public function __construct($ddb_config)
    {
        $this->config = $ddb_config;

        $this->credentials_array = array(
            'region' => $ddb_config['aws']['region'],
            'version' => $ddb_config['aws']['version'],
            'credentials' => array(
                'key'    => $ddb_config['aws']['access_key_id'],
                'secret' => $ddb_config['aws']['secret_access_key'],
            )
        );
    }

    public function sessionExists($id)
    {
        $client = new DynamoDbClient($this->credentials_array);

        $response = $client->getItem ( array (
            "TableName" => $this->config['sessions_table'],
            "ConsistentRead" => true,
            "Key" => array (
                "session_id" => array (
                    "S" => $id
                )
            )
        ) );
        return !empty($response["Item"]);
    }

    public function saveSession($data)
    {
        $client = new DynamoDbClient($this->credentials_array);

        $session_id = uniqid("sesh_");

        $item = array(
            'session_id' => array("S" => $session_id),
            'created' => array("N" => (string) time())
        );

        /*foreach ($data as $key => $val) {
            $item[$key] = array(
                "S" => $val
            );
        }*/

        $client->putItem(array(
            "TableName" => $this->config['sessions_table'],
            "Item" => $item
        ));

        return $session_id;
    }

    public function saveEvent($session_id, $action, $data)
    {
        $client = new DynamoDbClient($this->credentials_array);

        $item = array(
            'event_id' => array("N" => str_replace('.', '', microtime(true)) . rand(0, 10000)),
            'session_id' => array("S" => $session_id),
            'action' => array("S" => $action),
            'created' => array("N" => (string) time())
        );

        /*foreach ($data as $key => $val) {
            $item[$key] = array(
                "S" => $val
            );
        }*/

        $client->putItem(array(
            "TableName" => $this->config['events_table'],
            "Item" => $item
        ));

        return true;
    }
}
