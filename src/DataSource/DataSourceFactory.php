<?php

namespace PB\PIV\TrackingService\DataSource;

class DataSourceFactory
{
    public static function get($datasource_config)
    {
        $class = "\\PB\\PIV\\TrackingService\\DataSource\\" . $datasource_config['class'];
        return new $class($datasource_config);
    }
}