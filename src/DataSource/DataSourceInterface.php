<?php

namespace PB\PIV\TrackingService\DataSource;

interface DataSourceInterface
{
    public function sessionExists($id);

    public function saveSession($data);

    public function saveEvent($session_id, $action, $data);
}
