<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use PB\PIV\TrackingService\Config\ConfigFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PB\PIV\TrackingService\Session;
use PB\PIV\TrackingService\Event;

$app = new Silex\Application();

$app['debug'] = true;

$app->post('/event/track', function (Request $request, \Silex\Application $app) {
    // Sanitize and validate input
    $project_name = $app->escape($request->get('project'));
    $session_id = $app->escape($request->get('session'));
    $action = $app->escape($request->get('action'));
    $data = $request->get('data');
    $data = empty($data) ? array() : $data;

    $options = array(
        'demo' => array(
            'datasource' => array(
                'class' => 'DynamoDB',
                'sessions_table' => 'testSessions',
                'events_table' => 'testEvents',
                'aws' => array(
                    'region' => 'eu-west-1',
                    'version' => 'latest',
                    'access_key_id' => 'AKIAJ3EG5ZYIPH5TJ6HQ',
                    'secret_access_key' => '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'
                )
            ),
            'valid_actions' => array(
                'tracking-action-one',
                'tracking-action-two',
                'tracking-action-four',
            )
        )
    );
    $config = ConfigFactory::get('PhpArray', $options);

    if (empty($project_name) OR empty($session_id) OR empty($action) OR !is_array($data)) {
        return new Response(json_encode(array('msg' => 'Incorrect Usage')), 400);
    }

    // Check project exists
    if ( !$config->projectExists($project_name)) {
        return new Response(json_encode(array('msg' => 'Invalid Project')), 400);
    }

    // Check session
    $session = new Session($project_name, $config);
    if ( !$session->exists($session_id)) {
        return new Response(json_encode(array('msg' => 'Invalid Session')), 400);
    }

    // Check action
    $event = new Event($project_name, $config);
    if ( !$event->validAction($action)) {
        return new Response(json_encode(array('msg' => 'Invalid Action')), 400);
    }

    // Save event
    if ( !$event->save($session_id, $action, $data)) {
        return new Response(json_encode(array('msg' => 'Something went wrong with your request')), 500);
    }

    return new Response(json_encode(array('msg' => 'Event tracked successfully')), 200);
});

$app->post('/session/create', function (Request $request, \Silex\Application $app) {
    // Sanitize and validate input
    $project_name = $app->escape($request->get('project'));
    $data = $request->get('data');
    $data = empty($data) ? array() : $data;

    $options = array(
        'demo' => array(
            'datasource' => array(
                'class' => 'DynamoDB',
                'sessions_table' => 'testSessions',
                'events_table' => 'testEvents',
                'aws' => array(
                    'region' => 'eu-west-1',
                    'version' => 'latest',
                    'access_key_id' => 'AKIAJ3EG5ZYIPH5TJ6HQ',
                    'secret_access_key' => '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'
                )
            ),
            'valid_actions' => array(
                'tracking-action-one',
                'tracking-action-two',
                'tracking-action-four',
            )
        )
    );
    $config = ConfigFactory::get('PhpArray', $options);

    if (empty($project_name) OR !is_array($data)) {
        return new Response(json_encode(array('msg' => 'Incorrect Usage')), 400);
    }

    // Check project exists
    if ( !$config->projectExists($project_name)) {
        return new Response(json_encode(array('msg' => 'Invalid Project')), 400);
    }

    // Save session
    $session = new Session($project_name, $config);
    $session_id = $session->save($data);
    if ( !$session_id) {
        return new Response(json_encode(array('msg' => 'Something went wrong with your request')), 500);
    }

    return new Response(json_encode(
        array(
            'msg' => 'Session created successfully',
            'session_id' => $session_id
        )
    ), 200);
});

$app->after(function (Request $request, Response $response) {
    $response->headers->set('Access-Control-Allow-Origin', '*');
});

$app->run();